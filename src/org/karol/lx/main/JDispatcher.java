/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.main;

import java.io.FileInputStream;
import java.io.IOException;
import org.karol.lx.distributor.WorkDistributor;
import org.karol.lx.fetcher.RequestFetcher;
import org.karol.lx.fetcher.RequestFetcherConfigurationReader;
import org.karol.lx.monitor.Monitor;
import org.karol.lx.monitor.MonitorConfigurationReader;
import org.karol.lx.support.Configuration;

/**
 * Entry point utama JDispatcher
 * @author karol
 */
public class JDispatcher {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        Configuration.getInstance().load(new FileInputStream("conf/default.conf"));
        
        Monitor.getInstance().loadConfiguration(new MonitorConfigurationReader(Configuration.getInstance()));
        Monitor.getInstance().prepare();
        Monitor.getInstance().main();
        
        WorkDistributor.getInstance().setGraderSelector(Monitor.getInstance());
        WorkDistributor.getInstance().main();
        
        RequestFetcher.getInstance().openDBConnection(new RequestFetcherConfigurationReader(Configuration.getInstance()));
        RequestFetcher.getInstance().setReceiver(WorkDistributor.getInstance());
        RequestFetcher.getInstance().main();
    }
}
