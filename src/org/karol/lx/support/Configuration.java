/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.support;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author karol
 */
public class Configuration extends Properties {

    private static Configuration instance = null;

    private Configuration() {
    }

    public static Configuration getInstance() {
        if (instance == null) {
            instance = new Configuration();
        }
        return instance;
    }

    public Long getLong(String pKey) throws ConfigurationException {
        if (containsKey(pKey)) {
            return Long.valueOf(get(pKey).toString());
        } else {
            throw new ConfigurationException("Key " + pKey + " not found");
        }
    }

    public String getString(String pKey) throws ConfigurationException {
        if (containsKey(pKey)) {
            return get(pKey).toString();
        } else {
            throw new ConfigurationException("Key " + pKey + " not found");
        }
    }

    public Integer getInteger(String pKey) throws ConfigurationException {
        if (containsKey(pKey)) {
            return Integer.valueOf(get(pKey).toString());
        } else {
            throw new ConfigurationException("Key " + pKey + " not found");
        }
    }
}
