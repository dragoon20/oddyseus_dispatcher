/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.monitor;

import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.karol.lx.distributor.DistributingStrategy;
import org.karol.lx.grader.StandardGraderInterface;
import org.karol.lx.support.Configuration;
import org.karol.lx.support.ConfigurationException;

/**
 *
 * @author karol
 */
public class MonitorConfigurationReader {

    protected Configuration mConfiguration;

    public MonitorConfigurationReader(Configuration pConfiguration) {
        mConfiguration = pConfiguration;
    }

    public ArrayList<StandardGraderInterface> getRegisteredGraderInterfaces() {
        ArrayList<StandardGraderInterface> retval = new ArrayList<StandardGraderInterface>();
        try {
            Integer tMachineCount = mConfiguration.getInteger("machine-count");

            for (int i = 0; i < tMachineCount; i++) {
                String tDirectory = Configuration.getInstance().getString("directory" + (i + 1));
                StandardGraderInterface a = new StandardGraderInterface(InetAddress.getByName(Configuration.getInstance().getString("machine-address" + (i + 1))) , tDirectory);
                retval.add(a);
            }
        } catch (ConfigurationException ex) {
            Logger.getLogger(Monitor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnknownHostException ex) {
            Logger.getLogger(Monitor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retval;
    }

    public DistributingStrategy getSelectedStrategy() {
        DistributingStrategy retval = null;

        String strategyName;
        try {
            strategyName = Configuration.getInstance().getString("balancing-strategy");
            retval = (DistributingStrategy) Class.forName("org.karol.lx.distributor." + strategyName).getConstructor().newInstance();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        } catch (InvocationTargetException ex) {
            ex.printStackTrace();
        } catch (NoSuchMethodException ex) {
            ex.printStackTrace();
        } catch (SecurityException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (ConfigurationException ex) {
            ex.printStackTrace();
        }

        Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Using strategy " + retval.getClass().getName());
        return retval;
    }
}
