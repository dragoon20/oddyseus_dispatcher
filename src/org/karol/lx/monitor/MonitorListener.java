/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.monitor;

import org.karol.lx.grader.StandardGraderInterface;

/**
 * Interface untuk event Endpoint monitor
 * @author karol
 */
public interface MonitorListener {
    public void onGraderInaccessible(StandardGraderInterface i); ///Physically off
}
