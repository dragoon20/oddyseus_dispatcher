/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.model;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.karol.lx.fetcher.RequestFetcher;

/**
 *
 * @author karol
 */
public class GradeRequest implements Serializable {

    public int id;
    public int action;
    public int status;
    public int mode;
    public Date m_DispatchTime;
    private int m_RequeueCount;

    public GradeRequest() {
        m_RequeueCount = 0;
    }

    public static ArrayList<GradeRequest> buildFromResultSet(ResultSet pSet) {
        ArrayList<GradeRequest> retval = new ArrayList<GradeRequest>();
        try {
            while (pSet.next()) {
                GradeRequest tRequest = new GradeRequest();

                for (Field f : GradeRequest.class.getDeclaredFields()) {
                    if (!f.getName().startsWith("m_")) {
                        try {
                            f.set(tRequest, pSet.getInt(f.getName()));
                        } catch (IllegalArgumentException ex) {
                            Logger.getLogger(GradeRequest.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IllegalAccessException ex) {
                            Logger.getLogger(GradeRequest.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }

                retval.add(tRequest);
            }
        } catch (SQLException ex) {
            Logger.getLogger(GradeRequest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return retval;
    }
    
    public int getRequeueCount() {
        return m_RequeueCount;
    }
    
    public void requeue() {
        m_RequeueCount++;
        try {
            Statement tStatement = RequestFetcher.getInstance().getConnection().createStatement();
            tStatement.executeUpdate("UPDATE `grade_requests` SET `status` = 1 WHERE `id` = " + id + ";");
        } catch (SQLException ex) {
            Logger.getLogger(RequestFetcher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void dispose() {
        try {
            Statement tStatement = RequestFetcher.getInstance().getConnection().createStatement();
            //Dummy
            tStatement.executeUpdate("UPDATE `grade_requests` SET `status` = 3, `receive_time` = NOW() WHERE `id` = " + id + ";");
            //tStatement.executeUpdate("UPDATE `grade_requests` SET `receive_time` = NOW() WHERE `id` = " + pRequest.id + ";");
        } catch (SQLException ex) {
            Logger.getLogger(RequestFetcher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void take() {
        try {
            Statement tStatement = RequestFetcher.getInstance().getConnection().createStatement();
            //Dummy
            tStatement.executeUpdate("UPDATE `grade_requests` SET `status` = 1, `receive_time` = NOW() WHERE `id` = " + id + ";");
            //tStatement.executeUpdate("UPDATE `grade_requests` SET `receive_time` = NOW() WHERE `id` = " + pRequest.id + ";");
        } catch (SQLException ex) {
            Logger.getLogger(RequestFetcher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String toString() {
        return id + ";" + action + ";" + mode + ";" + status;
    }
}
