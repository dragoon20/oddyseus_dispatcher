/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.fetcher;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.karol.lx.support.Configuration;
import org.karol.lx.support.ConfigurationException;

/**
 *
 * @author karol
 */
public class RequestFetcherConfigurationReader {

    private Configuration mConfiguration;

    public RequestFetcherConfigurationReader(Configuration pConfig) {
        mConfiguration = pConfig;
    }

    public String getDatabaseAddress() {
        try {
            return mConfiguration.getString("db-address");
        } catch (ConfigurationException ex) {
            Logger.getLogger(RequestFetcherConfigurationReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public String getDatabaseUser() {
        try {
            return mConfiguration.getString("db-user");
        } catch (ConfigurationException ex) {
            Logger.getLogger(RequestFetcherConfigurationReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public String getDatabasePassword() {
        try {
            return mConfiguration.getString("db-password");
        } catch (ConfigurationException ex) {
            Logger.getLogger(RequestFetcherConfigurationReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
}
