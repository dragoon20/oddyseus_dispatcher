/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.grader;

import org.karol.lx.model.GradeRequest;

/**
 *
 * @author karol
 */
public interface GraderEventListener {
    public void onGradingSuccess(GradeRequest pRequest);
    public void onGradingFailed(GradeRequest pRequest , Object pReason , boolean disposable);
}
