/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.distributor;

import org.karol.lx.grader.StandardGraderInterface;

/**
 *
 * @author karol
 */
public interface GraderSelector {

    public abstract StandardGraderInterface getSelectedGrader();
}
