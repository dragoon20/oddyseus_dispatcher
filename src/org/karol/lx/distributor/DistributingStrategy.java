/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.distributor;

import java.util.ArrayList;
import org.karol.lx.grader.StandardGraderInterface;

/**
 * Kelas abstrak strategi load balancing.
 *
 * @author karol
 */
public interface DistributingStrategy {
    
    public static final int MAX_HANDLED_REQUEST = 8;

    public StandardGraderInterface getRecommendedGrader(ArrayList<StandardGraderInterface> pAgents);
}
