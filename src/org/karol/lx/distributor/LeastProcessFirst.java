/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.distributor;

import java.util.ArrayList;
import org.karol.lx.grader.StandardGraderInterface;

/**
 * Balancing with greedy algorithms.
 * Mencari mesin dengan jumlah handled request terkecil
 * @author karol
 */
public class LeastProcessFirst implements DistributingStrategy {

    public LeastProcessFirst() {
    }

    @Override
    public StandardGraderInterface getRecommendedGrader(ArrayList<StandardGraderInterface> pAgents) {
        StandardGraderInterface retval = null;
        for (StandardGraderInterface a : pAgents) {
            if (a.isActive() && a.getHandledRequests().size() < MAX_HANDLED_REQUEST && (retval == null || a.getHandledRequests().size() < retval.getHandledRequests().size()))
                retval = a;
        }
        return retval;
    }
}
