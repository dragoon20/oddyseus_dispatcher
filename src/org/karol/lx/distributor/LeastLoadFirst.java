/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.distributor;

import java.util.ArrayList;
import org.karol.lx.grader.StandardGraderInterface;

/**
 *
 * @author karol
 */
public class LeastLoadFirst implements DistributingStrategy {

    public LeastLoadFirst() {
    }

    @Override
    public StandardGraderInterface getRecommendedGrader(ArrayList<StandardGraderInterface> pAgents) {
        StandardGraderInterface retval = null;
        for (StandardGraderInterface a : pAgents) {
            double minLoadPercentage = (retval == null) ? 1000.00 : retval.getLoadPercentage();
            double tCurrentLoadPercentage = a.getLoadPercentage();
            if (a.isActive() && a.getHandledRequests().size() <= MAX_HANDLED_REQUEST && (retval == null || (tCurrentLoadPercentage >= 0.00 && tCurrentLoadPercentage < minLoadPercentage && tCurrentLoadPercentage <= 80.00))) {
                retval = a;
            }
        }
        return retval;
    }
}
