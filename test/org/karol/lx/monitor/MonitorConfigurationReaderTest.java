/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.monitor;

import java.io.FileInputStream;
import java.io.IOException;
import org.junit.*;
import static org.junit.Assert.*;
import org.karol.lx.distributor.LeastProcessFirst;
import org.karol.lx.support.Configuration;

/**
 *
 * @author karol
 */
public class MonitorConfigurationReaderTest {
    
    private MonitorConfigurationReader instance;
    
    public MonitorConfigurationReaderTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() throws IOException {
        Configuration.getInstance().load(new FileInputStream("conf/default.conf"));
        instance = new MonitorConfigurationReader(Configuration.getInstance());
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of getSelectedStrategy method, of class MonitorConfigurationReader.
     */
    @Test
    public void testGetSelectedStrategy() {
        System.out.println("getSelectedStrategy");
        assertEquals(instance.getSelectedStrategy().getClass(), LeastProcessFirst.class);
    }
}
