/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.monitor;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.*;
import static org.junit.Assert.*;
import org.karol.lx.grader.StandardGraderInterface;
import org.karol.lx.mock.GraderMock;
import org.karol.lx.mock.MonitorConfigurationReaderMock;

/**
 *
 * @author karol
 */
public class MonitorTest {
    
    private Monitor instance;
    private MonitorConfigurationReaderMock configuratorMock = new MonitorConfigurationReaderMock();
    
    public MonitorTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
        instance = Monitor.getInstance();
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testLoadConfiguration() {
        System.out.println("Test Load Configuration");
        instance.loadConfiguration(configuratorMock);
        assertEquals(configuratorMock.graders, instance.getGraderInterfaces());
        assertEquals(configuratorMock.strategy, instance.getStrategy());
    }
    
    @Test
    public void testEvaluationReachable() {
        System.out.println("Test evaluation (reachable)");
        instance.loadConfiguration(configuratorMock);
        instance.main();
        try {
            Thread.sleep(Monitor.MONITORING_PERIOD + 100);
        } catch (InterruptedException ex) {
            Logger.getLogger(MonitorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (StandardGraderInterface i : instance.getGraderInterfaces()) {
            assertEquals(i.getLoadPercentage() , 30.00 , 1.00);
        }
    }
    
    @Test
    public void testEvaluationUnreachable() {
        System.out.println("Test evaluation (unreachable)");
        instance.loadConfiguration(configuratorMock);
        for (StandardGraderInterface i : instance.getGraderInterfaces()) {
            ((GraderMock)i).loadInformationMode = GraderMock.LOAD_INFO_MODE_UNREACHABLE;
        }
        instance.main();
        try {
            Thread.sleep(Monitor.MONITORING_PERIOD + 100);
        } catch (InterruptedException ex) {
            Logger.getLogger(MonitorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (StandardGraderInterface i : instance.getGraderInterfaces()) {
            assertEquals(((GraderMock)i).requeued , true);
        }
    }
}
