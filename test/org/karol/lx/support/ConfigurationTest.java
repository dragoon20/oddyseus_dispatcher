/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.support;

import java.io.FileInputStream;
import java.io.IOException;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author karol
 */
public class ConfigurationTest {
    
    public ConfigurationTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() throws IOException {
        Configuration.getInstance().load(new FileInputStream("conf/default.conf"));
    }
    
    @After
    public void tearDown() {
    }


    /**
     * Test of getLong method, of class Configuration.
     */
    @Test
    public void testGetLong() throws Exception {
        System.out.println("getLong");
        assertEquals(Configuration.getInstance().getLong("machine-count"), Long.valueOf(1));
    }

    /**
     * Test of getString method, of class Configuration.
     */
    @Test
    public void testGetString() throws Exception {
        System.out.println("getString");
        assertEquals(Configuration.getInstance().getString("db-user"), "root");
    }

    /**
     * Test of getInteger method, of class Configuration.
     */
    @Test
    public void testGetInteger() throws Exception {
        System.out.println("getInteger");
        assertEquals(Configuration.getInstance().getInteger("machine-count"), Integer.valueOf(1));
    }

}
