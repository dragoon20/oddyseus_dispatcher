/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.mock;

import java.net.InetAddress;
import org.karol.lx.grader.StandardGraderInterface;
import org.karol.lx.model.GradeRequest;

/**
 *
 * @author karol
 */
public class GraderMock extends StandardGraderInterface {

    public GradeRequest receivedRequest = null;
    public int loadInformationMode = LOAD_INFO_MODE_REACHABLE;
    public static final int LOAD_INFO_MODE_REACHABLE = 0;
    public static final int LOAD_INFO_MODE_UNREACHABLE = 1;
    public boolean requeued = false;

    public GraderMock(InetAddress mAddress) {
        super(mAddress);
    }

    @Override
    public boolean offerRequest(GradeRequest pRequest) {
        receivedRequest = pRequest;
        return true;
    }

    @Override
    public synchronized double getActualLoadPercentage() {
        if (loadInformationMode == LOAD_INFO_MODE_REACHABLE) {
            return 30.00;
        } else {
            return -1.00;
        }
    }

    @Override
    public synchronized void requeueHandledRequests() {
        requeued = true;
    }
}
