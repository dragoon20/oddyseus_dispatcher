/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.mock;

import org.karol.lx.distributor.WorkDistributor;
import org.karol.lx.fetcher.RequestReceiver;
import org.karol.lx.model.GradeRequest;

/**
 *
 * @author karol
 */
public class WorkDistributorMock implements RequestReceiver {

    public GradeRequest receivedRequest = null;
    
    public WorkDistributorMock() {
    }

    @Override
    public void addRequest(GradeRequest pRequest) {
        receivedRequest = pRequest;
    }
}
