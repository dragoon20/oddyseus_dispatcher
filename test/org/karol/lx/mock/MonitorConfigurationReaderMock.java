/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.mock;

import java.util.ArrayList;
import org.karol.lx.distributor.DistributingStrategy;
import org.karol.lx.distributor.LeastLoadFirst;
import org.karol.lx.distributor.LeastProcessFirst;
import org.karol.lx.grader.StandardGraderInterface;
import org.karol.lx.monitor.MonitorConfigurationReader;

/**
 *
 * @author karol
 */
public class MonitorConfigurationReaderMock extends MonitorConfigurationReader {

    public GraderMock g1 = new GraderMock(null), g2 = new GraderMock(null);
    public ArrayList<StandardGraderInterface> graders = new ArrayList<StandardGraderInterface>();
    public DistributingStrategy strategy = new LeastLoadFirst();

    public MonitorConfigurationReaderMock() {
        super(null);
    }

    @Override
    public ArrayList<StandardGraderInterface> getRegisteredGraderInterfaces() {
        graders.add(g1);
        graders.add(g2);
        return graders;
    }

    @Override
    public DistributingStrategy getSelectedStrategy() {
        return strategy;
    }
}
