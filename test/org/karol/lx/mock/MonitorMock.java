/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.mock;

import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.karol.lx.distributor.GraderSelector;
import org.karol.lx.grader.StandardGraderInterface;

/**
 *
 * @author karol
 */
public class MonitorMock implements GraderSelector {
    
    public int mode = MODE_AVAILABLE;
    
    public static final int MODE_AVAILABLE = 0;
    public static final int MODE_UNAVAILABLE = 1;
    
    public GraderMock g1,g2;

    public MonitorMock() {
        try {
            g1 = new GraderMock(Inet4Address.getByName("127.0.0.1"));
            g2 = new GraderMock(Inet4Address.getByName("127.0.0.1"));
        } catch (UnknownHostException ex) {
            Logger.getLogger(MonitorMock.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public StandardGraderInterface getSelectedGrader() {
        if (mode == MODE_AVAILABLE) {
            return g1;
        }
        else {
            return null;
        }
    }
    
}
