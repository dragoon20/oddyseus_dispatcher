/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.distributor;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.*;
import static org.junit.Assert.*;
import org.karol.lx.mock.MonitorMock;
import org.karol.lx.model.GradeRequest;

/**
 *
 * @author karol
 */
public class WorkDistributorTest {

    private WorkDistributor instance;
    private MonitorMock monitorMock;

    public WorkDistributorTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        instance = WorkDistributor.getInstance();

        monitorMock = new MonitorMock();
        instance.setGraderSelector(monitorMock);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testRequestReceivedAvailable() {
        System.out.println("Test request received");
        instance.main();
        GradeRequest pRequest = new GradeRequest();
        instance.addRequest(pRequest);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(WorkDistributorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals(pRequest, monitorMock.g1.receivedRequest);
    }

    @Test
    public void testRequestReceivedUnavailable() {
        System.out.println("Test request received");
        instance.main();
        monitorMock.mode = MonitorMock.MODE_UNAVAILABLE;
        GradeRequest pRequest = new GradeRequest();
        instance.addRequest(pRequest);
        try {
            Thread.sleep(4000);
        } catch (InterruptedException ex) {
            Logger.getLogger(WorkDistributorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        monitorMock.mode = MonitorMock.MODE_AVAILABLE;
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(WorkDistributorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertEquals(pRequest, monitorMock.g1.receivedRequest);
    }
}
