/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.fetcher;

import java.io.FileInputStream;
import java.io.IOException;
import org.junit.*;
import static org.junit.Assert.*;
import org.karol.lx.support.Configuration;

/**
 *
 * @author karol
 */
public class RequestFetcherConfigurationReaderTest {
    
    private RequestFetcherConfigurationReader instance;
    
    public RequestFetcherConfigurationReaderTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() throws IOException {
        Configuration.getInstance().load(new FileInputStream("conf/default.conf"));
        instance = new RequestFetcherConfigurationReader(Configuration.getInstance());
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getDatabaseAddress method, of class RequestFetcherConfigurationReader.
     */
    @Test
    public void testGetDatabaseAddress() {
        System.out.println("getDatabaseAddress");
        assertEquals(instance.getDatabaseAddress(), "jdbc:mysql://localhost/lz_db");
    }

    /**
     * Test of getDatabaseUser method, of class RequestFetcherConfigurationReader.
     */
    @Test
    public void testGetDatabaseUser() {
        System.out.println("getDatabaseUser");
        assertEquals(instance.getDatabaseUser(), "root");
    }

    /**
     * Test of getDatabasePassword method, of class RequestFetcherConfigurationReader.
     */
    @Test
    public void testGetDatabasePassword() {
        System.out.println("getDatabasePassword");
        assertEquals(instance.getDatabasePassword(), "karolkarol");
    }
}
