/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.karol.lx.fetcher;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.*;
import static org.junit.Assert.*;
import org.karol.lx.distributor.WorkDistributor;
import org.karol.lx.mock.WorkDistributorMock;
import org.karol.lx.model.GradeRequest;
import org.karol.lx.monitor.Monitor;
import org.karol.lx.monitor.MonitorConfigurationReader;
import org.karol.lx.support.Configuration;

/**
 *
 * @author karol
 */
public class RequestFetcherTest {

    private RequestFetcher instance;
    private RequestFetcherConfigurationReader reader;
    private WorkDistributorMock distributor;

    public RequestFetcherTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws IOException {
        Configuration.getInstance().load(new FileInputStream("conf/default.conf"));
        reader = new RequestFetcherConfigurationReader(Configuration.getInstance());
        instance = RequestFetcher.getInstance();
        distributor = new WorkDistributorMock();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testOpenDBConnection() {
        System.out.println("Test openDBConnection");
        instance.openDBConnection(reader);
    }

    @Test
    public void testFetchRequest() {
        System.out.println("Test Fetch Request");
        instance.openDBConnection(reader);
        instance.setReceiver(distributor);

        ArrayList<GradeRequest> tPendingRequests = instance.getPendingRequests();

        instance.main();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(RequestFetcherTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        GradeRequest receivedRequest = distributor.receivedRequest;
        boolean graded = false;
        for (GradeRequest r : tPendingRequests) {
            if (receivedRequest.id == r.id) {
                graded = true;
            }
        }
        assertTrue(graded);
    }
}
